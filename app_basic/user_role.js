/**
 * Created by Ruwan on 11/26/2017.
 */
const user_roles = {
  GENERAL: 1,
  ADMIN: 2
};

module.exports = user_roles;