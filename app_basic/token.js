/**
 * Created by Ruwan on 11/19/2017.
 */
var  encrypt = require('./encrypt');

module.exports = {
    new_token: function (user_id, role) {
        var token_json = {
            time:Date.parse(new Date().toString()),
            user_id: user_id,
            role: role
        };
        console.log(token_json)
        return encrypt.encrypt(JSON.stringify(token_json));
    },
    get_details: function (token) {
        return encrypt.decrypt(token);
    },
    get_token: function (req) {
        return req.headers.token;
    }
}