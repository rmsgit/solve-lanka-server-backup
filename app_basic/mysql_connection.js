/**
 * Created by Ruwan on 11/14/2017.
 */
var mysql      = require('mysql');
var send       = require('./send');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    database : 'solve_lanka',
    password : ''
    // password : ''
});

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

module.exports = {
    call:function(req, res, query, callback){
        console.log("call : ",query);
        connection.query(query, function (error, results, fields) {
            if (error) {
                console.log(error);
                switch (error.code){

                    case "ER_DUP_ENTRY":
                        send.error(req, res, 5002);
                        break;
                    default:
                        send.error(req, res, 5001)

                }

                // if(error.Error.ER_DUP_ENTRY){
                //
                // }else send.error(req, res, 5001)
            }
            callback(results);
        });
    }
}
