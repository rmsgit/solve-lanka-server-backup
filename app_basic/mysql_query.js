/**
 * Created by Ruwan on 11/17/2017.
 */
const tables =  {
    dislike: "dislike",
    down_rate: "down_rate",
    general_user: "general_user",
    like: "`like`",
    problem: "problem",
    solution: "solution",
    up_rate: "up_rate",
    log_traffic: "log_traffic",
    user: "user",
    log_404: "log_404",
    email: 'email'
};
var select = {
    all:'SELECT * ',
    cols:function (cols) {
        return 'SELECT '+ cols.join(', ')
    },
    sum: function (col){
        return 'SELECT SUM( ' + col + ' )';
    },
    row_count: function (col){
        return 'SELECT ROW_COUNT()';
    }
};
var insert = function (table, object) {
    var values = [];
    var cols = Object.keys(object);

    for (var i=0; i < cols.length; i++) {
       values.push(JSON.stringify(object[cols[i]]))
    }

    return 'INSERT INTO '+ table+ ' (' + cols.join(', ') + ') VALUES (' + values.join(', ') + ')'
};
var from = function (table) {
    return 'FROM ' + table;
};

var where = function (where) {
    return 'WHERE ' + where;
};
var update = function (table, sets) {
    return 'UPDATE  '+ table + ' SET '+ sets;
};
var query = function (q) {
    var query  =q.join(' ');
    return query;
};
var ascending = function(col) {
    return 'ORDER BY '+col+' ASC'
}
var descending = function(col) {
    return 'ORDER BY '+col+' DESC'
}
module.exports = {
    user: {
        all:query([
            select.all,
            from(tables.user)
        ]),
        form_email:function (email){
            return query([
                select.all,
                from(tables.user),
                where('email = "'+ email + '"')
            ])
        },
        form_token:function (token){
            return query([
                select.all,
                from(tables.user),
                where('token = "'+ token + '"')
            ])
        },
        set_token: function (token, user_id) {
            return query([
                update(tables.user, 'token = "'+ token + '"' ),
                where('id ='+ user_id)
            ])
        },
        add: function (object) {
            return query([
                insert(tables.user, object )
            ])
        }
    },
    general_user:{
        add: function (object) {
            return query([
                insert(tables.general_user, object )
            ])
        }
    },
    post: {
        all:query([
            select.cols([
                'id', 'title', 'text', 'time', 'rate'
            ]),
            from(tables.problem)
        ]),
        form_id:function(post_id){
            return query([
                select.cols([
                    'id', 'title', 'text', 'time', 'rate'
                ]),
                from(tables.problem),
                where('id ='+ post_id)
            ])
        },
        add:function (object) {
            return query([
                insert(tables.problem, object)
            ])

        },
        isLike: function (user_id, problem_id) {
            return query([
                select.cols(['id']),
                from(tables.like),
                where('problem_id =' + problem_id + ' AND user_id =' + user_id)
            ])
        },
        like: function (object) {
            return query([
                insert(tables.like, object)
            ])
        },
        isDislike: function (user_id, problem_id) {
            return query([
                select.cols(['id']),
                from(tables.dislike),
                where('problem_id =' + problem_id + ' AND user_id =' + user_id)
            ])
        },
        dislike: function (object) {
            return query([
                insert(tables.dislike, object)
            ])
        },
        likeCount: function (problem_id) {
            return query([
                select.cols([
                    'id'
                ]),
                from(tables.like),
                where('problem_id = ' + problem_id)
            ])
        },
        dislikeCount: function (problem_id) {
            return query([
                select.cols([
                    'id'
                ]),
                from(tables.dislike),
                where('problem_id = ' + problem_id)
            ])
        }

    },
    solution: {
        all:query([
            select.cols([
                'id', 'text', 'time', 'user_name'
            ]),
            from(tables.solution),
            where('state=' + 1)
        ]),
        form_id:function(post_id){
            return query([
                select.cols([
                    'id', 'text', 'time', 'user_name', 'rate'
                ]),
                from(tables.solution),
                where('problem_id ='+ post_id + ' AND state=' + 2),
                descending('rate')
            ])
        },
        add:function (object) {
            return query([
                insert(tables.solution, object)
            ])

        },
        isUpRate: function (user_id, solution_id) {
            return query([
                select.cols(['id']),
                from(tables.up_rate),
                where('solution_id =' + solution_id + ' AND user_id =' + user_id)
            ])
        },
        upRate: function (object) {
            return query([
                insert(tables.up_rate, object)
            ])
        },
        isDownRate: function (user_id, solution_id) {
            return query([
                select.cols(['id']),
                from(tables.down_rate),
                where('solution_id =' + solution_id + ' AND user_id =' + user_id)
            ])
        },
        downRate: function (object) {
            return query([
                insert(tables.down_rate, object)
            ])
        },
        increaseRateCount: function (solution_id){
            return query([
                update(tables.solution, 'rate=  rate + 1'),
                where('id='+solution_id)
            ])
        },
        dicreaseRateCount: function (solution_id){
            return query([
                update(tables.solution, 'rate=  rate  - 1'),
                where('id='+solution_id)
            ])
        },
        active: function (solution_id){
            return query([
                update(tables.solution, '`state`='+2),
                where('id='+solution_id)
            ])
        }
    },
    log:{
        traffic:function (object) {
            return query([
                insert(tables.log_traffic, object)
            ])
        },
        log404: function (object) {
            return query([
                insert(tables.log_404, object)
            ])
        }
    },
    email:{
        send: function (object) {
            return query([
                insert(tables.email, object)
            ])
        }
    }
};