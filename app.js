var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var iplocation = require('iplocation');

var send = require('./app_basic/send');
var index = require('./routes/index');
var users = require('./routes/users');
var auth = require('./routes/auth');
var post = require('./routes/post');
var solution = require('./routes/solution');
var problem_rate = require('./routes/problem_rate');
var log = require('./routes/log');
var email = require('./routes/mail');

const cors = require('cors');

var send = require('./app_basic/send');
var mysql_caller = require('./app_basic/mysql_connection');
var mysql_query = require('./app_basic/mysql_query');

var app = express();

const base_path = '/api/';
var getPath = function (sub_path) {
    return base_path + sub_path;
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cors());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.options(function (req, res) {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader('Access-Control-Allow-Methods', '*');
//     res.setHeader("Access-Control-Allow-Headers", "*");
//     res.end();
// });

app.use('/', index);
app.use(getPath('user'), users);
app.use(getPath('auth'), auth);
app.use(getPath('post'), post);
app.use(getPath('solution'), solution);
app.use(getPath('rate'), problem_rate);
app.use(getPath('log'), log);
app.use(getPath('mail'), email);
// catch 404 and forward to error handler
app.use(function(req, res, next) {

    var ip = req.connection.remoteAddress
    var url = req.url;

    
    iplocation(ip, (error, where_client) => {
        if(error){
            mysql_caller.call(req, res, mysql_query.log.log404({
                user_agent:req.headers['user-agent'],
                host: req.headers['host'],
                ip: ip
            }), function (result) {
                if(!url.startsWith('/api/') ||  !url.startsWith('/')){
                    res.sendfile(__dirname + '/public/index.html');
                }else send.error(req, res, 404 )
            })  
        }else mysql_caller.call(req, res, mysql_query.log.log404({
            user_agent:req.headers['user-agent'],
            host: req.headers['host'],
            ip: ip,
            user: JSON.stringify(where_client)
        }), function (result) {
            if(!url.startsWith('/api/') ||  !url.startsWith('/') ){
                res.sendfile(__dirname + '/public/index.html');
            }else send.error(req, res, 404 )
        })
    })

// send.error(req, res, 404)
   
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(err);
  // render the error page
  send.error(req, res, 500);
});

module.exports = app;
