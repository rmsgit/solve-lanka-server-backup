var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
/* GET users listing. */
router.post('/general', function(req, res, next) {

      if(validator.str([
              req.body.email,
              req.body.password,
              req.body.name,
              req.body.education
          ])){
          mysql_caller.call(req, res, mysql_query.user.add({
              email: req.body.email,
              password: req.body.password,
              name: req.body.name,
              role: role.GENERAL
          }), function (result) {
              if (result) {
                  if (result.affectedRows) {
                      mysql_caller.call(req, res, mysql_query.general_user.add({
                          user_id: result.insertId,
                          education: req.body.education
                      }), function (result) {
                          send.data(req, res, {
                              message: 'Successfully saved'
                          })
                      })
                  } else {
                      send.error(req, res, 5002)
                  }
              }

          });
      }else send.error(req, res, 400)
});

module.exports = router;
