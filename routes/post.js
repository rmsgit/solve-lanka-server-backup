/**
 * Created by Ruwan on 11/26/2017.
 */
var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
/* GET users listing. */
router.post('/', function(req, res, next) {
    var req_token = token.get_token(req);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        console.log(user)
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.post.add({
                title:req.body.title,
                text:req.body.text,
                user_id: user[0].id
            }),function (result) {
                send.data(req, res, {
                    insert_id:result.insertId
                });
            })
        }else {
            send.error(req,res, 401);
        }
    })

});
router.get('/', function(req, res, next) {
    mysql_caller.call(req, res, mysql_query.post.all,function (problems) {
        send.data(req, res, problems)
    })

});
router.get('/:post_id', function(req, res, next) {
    var post_id = req.params.post_id;

    mysql_caller.call(req, res, mysql_query.post.form_id(post_id),function (problems) {
        send.data(req, res, problems[0])
    })
});
router.get('/rates/:post_id', function(req, res, next) {
    var post_id = req.params.post_id;
    mysql_caller.call(req, res, mysql_query.post.likeCount(post_id),function (likes) {
        mysql_caller.call(req, res, mysql_query.post.dislikeCount(post_id),function (dislikes) {
            send.data(req, res, {
                likes: likes.length,
                dislikes:dislikes.length
            })
         })
    })
});
module.exports = router;
