/**
 * Created by Ruwan on 11/26/2017.
 */
var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
/* GET users listing. */
router.post('/:problem_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var problem_id = parseInt(req.params.problem_id);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.solution.add({
                text:req.body.text,
                user_id: user[0].id,
                user_name: user[0].name,
                problem_id: problem_id
            }),function (result) {
                send.data(req, res, {
                    insert_id:result.insertId
                });
            })
        }else {
            send.error(req,res, 401);
        }
    })

});

router.post('/active/:solution_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var solution_id = parseInt(req.params.solution_id);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1 && user[0].role ==role.ADMIN) {
            mysql_caller.call(req, res, mysql_query.solution.active(solution_id),function (result) {
                send.data(req, res, {
                    message:"Activated"
                });
            })
        }else {
            send.error(req,res, 401);
        }
    })

});

router.get('/', function(req, res, next) {
    var req_token = token.get_token(req);
    var problem_id = parseInt(req.params.problem_id);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1  && user[0].role ==role.ADMIN) {
            mysql_caller.call(req, res, mysql_query.solution.all,function (result) {
                send.data(req, res, result);
            })
        }else {
            send.error(req,res, 401);
        }
    })
});

router.get('/:problem_id(\\d+)', function(req, res, next) {
    var problem_id = parseInt(req.params.problem_id);
    mysql_caller.call(req, res, mysql_query.solution.form_id(problem_id),function (solutions) {
        send.data(req, res, solutions)
    })

});

module.exports = router;
