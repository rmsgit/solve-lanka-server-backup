/**
 * Created by Ruwan on 11/17/2017.
 */
var express = require('express');
var send = require('../app_basic/send');
var valid = require('../app_basic/validator');
var token = require('../app_basic/token');
var mysql = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
var user_roles = require('../app_basic/user_role');
var router = express.Router();

router.post('/', function(req, res, next) {
    var email = req.body.email;
    var pass = req.body.password;
    if (valid.str([
            email,
            pass
        ])){
        mysql.call(req, res, mysql_query.user.form_email(email), function (result) {
            if(result.length > 0) {
                var user = result[0];
                var user_role = Object.keys(user_roles)[user.role-1]; 
                if(user.password ==  pass) {
                    var new_token = token.new_token(user.user_id, user.role);
                    mysql.call(req, res, mysql_query.user.set_token(new_token, user.id),function (result) {
                        if(result.affectedRows > 0 ) {
                            send.data(req, res, {
                                token: new_token,
                                user_role: user_role,
                                user_email:user.email
                            })
                        }else {
                            send.data(req, res, {
                                message: "Can not save token"
                            })
                        }

                    })

                }else send.data(req, res, {
                    error: "Incorrect password"
                })

            }else send.data(req, res, {
                error: "Invalid email"
            })


        })
    }else send.error(req, res, 400)

});
router.get('/:token', function(req, res, next) {
    var token = req.params.token;
    mysql.call(req, res, mysql_query.user.form_token(token),function (user) {
        if (user.length > 0) {
            send.data(req, res,{
                user_id: user[0].id,
                email: user[0].email,
                role_id: user[0].role,
            });
        }else {
            send.error(req, res,404);
        }

    })

});

module.exports = router;
