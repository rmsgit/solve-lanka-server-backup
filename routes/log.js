/**
 * Created by Ruwan on 11/27/2017.
 */
var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
/* GET users listing. */
router.post('/traffic', function(req, res, next) {
    mysql_caller.call(req, res, mysql_query.log.traffic({
        user_agent:req.headers['user-agent'],
        host: req.headers['host'],
    }), function (result) {
        send.error(req, res, 200 )
    })

});
router.get('/traffic', function(req, res, next) {
    mysql_caller.call(req, res, mysql_query.log.traffic({
        user_agent:req.headers['user-agent'],
        host: req.headers['host'],
    }), function (result) {
        send.error(req, res, 200 )
    })

});

module.exports = router;
