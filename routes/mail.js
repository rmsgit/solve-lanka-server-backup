/**
 * Created by Ruwan on 12/30/2017.
 */


var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
var mail = require('../app_basic/email');

router.post('/make_me_admin', function(req, res, next) {
    var req_token = token.get_token(req);
    var subject = "Make Me a admin";
    var html = req.body.html;

    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.email.send({
                "`from`":'reg@solvelanka.com',
                "`to`":'admin@solvelanka.com',
                "`text`": html,
                subject: subject,
            }), function (data) {
                send.data(req, res, 'SENT')
            })
        }else {
            send.error(req, res, 401)
        }
    });
});

router.post('/reports', function(req, res, next) {
    var req_token = token.get_token(req);
    var subject = "Reports";
    var html = req.body.html;

    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.email.send({
                "`from`":'reg@solvelanka.com',
                "`to`":'admin@solvelanka.com',
                "`text`": html,
                subject: subject,
            }), function (data) {
                send.data(req, res, 'SENT')
            })
        }else {
            send.error(req, res, 401)
        }
    });

});

router.post('/inform', function(req, res, next) {
    var req_token = token.get_token(req);
    var subject = "Inform Problems";
    var html = req.body.html;

    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.email.send({
                "`from`":'reg@solvelanka.com',
                "`to`":'admin@solvelanka.com',
                "`text`": html,
                subject: subject,
            }), function (data) {
                send.data(req, res, 'SENT')
            })
        }else {
            send.error(req, res, 401)
        }
    });

});

router.post('/send', function(req, res, next) {

    var ml = req.body.email;
    var subject = req.body.subject;
    var html = req.body.html;
    mysql_caller.call(req, res, mysql_query.email.send({
        "`from`":ml,
        "`to`":'admin@solvelanka.com',
        "`text`":html,
        subject: subject,
    }),function (data) {
       if(data) send.data(req, res, 'SENT')
    });
});



module.exports = router;