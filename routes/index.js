var express = require('express');
var send = require('../app_basic/send');
var mysql = require('../app_basic/mysql_connection');

var router = express.Router();

/* GET home page. */
router.get('/test', function(req, res, next) {
    mysql.query('SELECT * FROM sql12205044.test_table', function (error, results, fields) {
        if (error) throw error;
        send.data(req, res, results);
    });

});
router.get('/', function(req, res, next) {
    send.error(req, res, 5001);
});

module.exports = router;
