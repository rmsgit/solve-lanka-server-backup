/**
 * Created by Ruwan on 11/27/2017.
 */
var express = require('express');
var router = express.Router();
var token = require('../app_basic/token');
var send = require('../app_basic/send');
var role = require('../app_basic/user_role');
var validator = require('../app_basic/validator');
var mysql_caller = require('../app_basic/mysql_connection');
var mysql_query = require('../app_basic/mysql_query');
/* GET users listing. */
router.post('/like/:problem_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var problem_id = parseInt(req.params.problem_id);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.post.isLike(
                    user[0].id,
                    problem_id
                ),function (result) {
                if (result.length == 0){
                    mysql_caller.call(req, res, mysql_query.post.like({
                        user_id: user[0].id,
                        problem_id: problem_id
                    }), function (like_result) {
                        send.data(req, res, {
                            message: 'liked'
                        })
                    });
                }else send.data(req, res, {
                    message: 'Already liked'
                })

            })
        }else{
            send.error(req,res, 401);
        }
    })

});

router.post('/dislike/:problem_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var problem_id = parseInt(req.params.problem_id);
    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            mysql_caller.call(req, res, mysql_query.post.isDislike(
                user[0].id,
                problem_id
            ),function (result) {
                if (result.length == 0){
                    mysql_caller.call(req, res, mysql_query.post.dislike({
                        user_id: user[0].id,
                        problem_id: problem_id
                    }), function (dislike_result) {
                        send.data(req, res, {
                            message: 'disliked'
                        })
                    });
                }else send.data(req, res, {
                    message: 'Already disliked'
                })

            })
        }else{
            send.error(req,res, 401);
        }
    })

});

router.post('/up_rate/:solution_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var solution_id = parseInt(req.params.solution_id);

    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {
            let rate = 0;
            mysql_caller.call(req, res, mysql_query.solution.isUpRate(
                user[0].id,
                solution_id
            ),function (result) {
                if (result.length == 0){
                    mysql_caller.call(req, res, mysql_query.solution.upRate({
                        user_id: user[0].id,
                        solution_id: solution_id
                    }), function (dislike_result) {
                        mysql_caller.call(req, res, mysql_query.solution.increaseRateCount(solution_id ),
                        function(data){
                            send.data(req, res, {
                                message: 'Uprated'
                            })
                        });
                    });
                }else send.data(req, res, {
                    message: 'Already uprated'
                })

            })
        }else{
            send.error(req,res, 401);
        }
    })
});

router.post('/down_rate/:solution_id(\\d+)', function(req, res, next) {
    var req_token = token.get_token(req);
    var solution_id = parseInt(req.params.solution_id);

    mysql_caller.call(req, res, mysql_query.user.form_token(req_token),function (user) {
        if (user.length == 1) {

            mysql_caller.call(req, res, mysql_query.solution.isDownRate(
                user[0].id,
                solution_id
            ),function (result) {
                if (result.length == 0){
                    mysql_caller.call(req, res, mysql_query.solution.downRate({
                        user_id: user[0].id,
                        solution_id: solution_id
                    }), function (dislike_result) {
                        mysql_caller.call(req, res, mysql_query.solution.dicreaseRateCount(solution_id ),
                        function(data){
                            send.data(req, res, {
                                message: 'down rate'
                            })
                        });
                    });
                }else send.data(req, res, {
                    message: 'Already down rated'
                })

            })
        }else{
            send.error(req,res, 401);
        }
    })
});

module.exports = router;
